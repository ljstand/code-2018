#LJ-STAND CODE

Git repository for Team LJ-STAND's Code in 2018.

------------------------------------------------

###Folder Structure
lib: Libraries

master: main program file for the master Teensy

slave_motor: main program file for the motor slave Teensy

slave_sensor: main program file for the sensor slave Teensy

slave_debug: main program file for the debug slave Teensy
